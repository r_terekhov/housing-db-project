package com.example.bd.ui.campaign;

import com.example.bd.model.Campaign;
import com.example.bd.repository.CampaignRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("campaign")
public class CampaignWebView extends VerticalLayout {

    private final CampaignRepository campaignRepository;

    private final CampaignEditor editor;

    private final Grid<Campaign> grid;

    private final TextField filter;

    private final Button addNewBtn;

    public CampaignWebView(CampaignRepository campaignRepository, CampaignEditor editor) {
        this.campaignRepository = campaignRepository;
        this.editor = editor;
        this.grid = new Grid<>(Campaign.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New campaign", VaadinIcon.PLUS.create());

        // build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("300px");
        grid.setColumns("campaignId", "name");
        grid.getColumnByKey("campaignId").setWidth("100px").setFlexGrow(0);

        filter.setPlaceholder("Filter by name");

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listOpinions(e.getValue()));

        // Connect selected Customer to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> editor.editCampaign(e.getValue()));

        // Instantiate and edit new Customer the new button is clicked
        addNewBtn.addClickListener(e -> editor.editCampaign(new Campaign()));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listOpinions(filter.getValue());
        });

        // Initialize listing
        listOpinions(null);
    }


    private void listOpinions(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(campaignRepository.findAll());
        } else {
            grid.setItems(campaignRepository.findAllByNameStartingWith(filterText));
        }
    }
}
