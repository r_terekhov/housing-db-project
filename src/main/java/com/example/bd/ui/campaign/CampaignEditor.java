package com.example.bd.ui.campaign;

import com.example.bd.model.Campaign;
import com.example.bd.repository.CampaignRepository;
import com.example.bd.ui.BaseEditor;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToLongConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.DateTimeFormatter;
import java.util.Collections;

@SpringComponent
@UIScope
public class CampaignEditor extends VerticalLayout implements KeyNotifier, BaseEditor {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    private final CampaignRepository campaignRepository;
    private ChangeHandler changeHandler;


    private Campaign campaign;

    /*FIELDS*/
    private TextField campaignIdField = new TextField("Campaign id");
    private TextField nameField = new TextField("Campaign Name");


    /*BUTTONS*/
    private Button save = new Button("Save", VaadinIcon.CHECK.create());
    private Button update = new Button("Update", VaadinIcon.UPLOAD.create());
    private Button cancel = new Button("Cancel");
    private Button delete = new Button("Delete", VaadinIcon.TRASH.create());


    private HorizontalLayout actions = new HorizontalLayout(save, cancel, delete, update);

    Binder<Campaign> binder = new Binder<>(Campaign.class);


    @Autowired
    public CampaignEditor(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;

        add(campaignIdField, nameField, actions);

        // bind using naming convention
       /* binder.forField(this.markField)
                .withNullRepresentation("")
                .withConverter(new StringToIntegerConverter(1, "integers only"))
                .bind(Opinion::getMark, Opinion::setMark);*/

        binder.forField(this.campaignIdField)
                .withConverter(new StringToLongConverter("error"))
                .bind(Campaign::getCampaignId, Campaign::setCampaignId);


        binder.bindInstanceFields(this);

        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());
        addKeyPressListener(Key.ESCAPE, e -> changeHandler.onChange());


        save.addClickListener(e -> save());
        update.addClickListener(e -> update());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editCampaign(campaign));
        setVisible(false);
    }


    @Override
    public void save() {
        initFromFields();
        campaignRepository.save(campaign);
        changeHandler.onChange();
    }


    @Override
    public void delete() {
        campaignRepository.deleteInBatch(Collections.singleton(campaign));
        changeHandler.onChange();
    }


    @Override
    public void update() {
        initFromFields();
        campaignRepository.save(campaign);
        changeHandler.onChange();
    }


    public final void editCampaign(Campaign campaign) {
        if (campaign == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = campaign.getCampaignId() != null;
        if (persisted) {
            this.campaign = campaignRepository.findById(campaign.getCampaignId()).get();
        } else {
            this.campaign = campaign;
        }

        // cancel.setVisible(persisted);

        cancel.setVisible(true);


        //binder.setBean(this.campaign);
        customSetupFields();


        setVisible(true);

        // Focus first name initially
        nameField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        changeHandler = h;
    }


    @Override
    public void customSetupFields() {
        if (campaign == null) {
            return;
        }

        if (campaign.getCampaignId() != null) {
            campaignIdField.setValue(campaign.getCampaignId().toString());
        }

        if (campaign.getName() != null) {
            nameField.setValue(campaign.getName());
        }


    }

    @Override
    public void initFromFields() {
        campaign.setName(nameField.getValue());
    }

    public interface ChangeHandler {
        void onChange();
    }
}
