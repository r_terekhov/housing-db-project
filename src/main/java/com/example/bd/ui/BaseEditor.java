package com.example.bd.ui;

public interface BaseEditor {

    void save();

    void update();

    void delete();

    void customSetupFields();

    void initFromFields();
}
