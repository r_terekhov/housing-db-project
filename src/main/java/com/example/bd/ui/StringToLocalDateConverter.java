package com.example.bd.ui;

import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class StringToLocalDateConverter
        implements Converter<String, LocalDate> {
    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    @Override
    public Result<LocalDate> convertToModel(String value,
                                            ValueContext context) {
        if (value == null) {
            return Result.ok(null);
        }
        return Result.ok(LocalDate.parse(value, formatter));
    }

    @Override
    public String convertToPresentation(LocalDate value,
                                        ValueContext context) {
        if (value == null) {
            return LocalDate.now().format(formatter);
        }

        return value.format(formatter);
    }
}