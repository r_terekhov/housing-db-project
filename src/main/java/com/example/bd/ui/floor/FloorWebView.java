package com.example.bd.ui.floor;

import com.example.bd.model.Floor;
import com.example.bd.repository.FloorRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("floor")
public class FloorWebView extends VerticalLayout {

    private final FloorRepository floorRepository;

    private final FloorEditor editor;

    private final Grid<Floor> grid;

    private final TextField filter;

    private final Button addNewBtn;

    public FloorWebView(FloorRepository floorRepository, FloorEditor editor) {
        this.floorRepository = floorRepository;
        this.editor = editor;
        this.grid = new Grid<>(Floor.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New floor", VaadinIcon.PLUS.create());

        // build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("300px");
        grid.setColumns("floorId", "floorNumber", "housing", "rooms");
        grid.getColumnByKey("floorId").setWidth("100px").setFlexGrow(0);

        filter.setPlaceholder("Filter by ...");

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listRooms(e.getValue()));

        // Connect selected Customer to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> editor.editFloor(e.getValue()));

        // Instantiate and edit new Customer the new button is clicked
        addNewBtn.addClickListener(e -> editor.editFloor(new Floor()));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listRooms(filter.getValue());
        });

        // Initialize listing
        listRooms(null);
    }


    private void listRooms(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(floorRepository.findAll());
        }
    }

}
