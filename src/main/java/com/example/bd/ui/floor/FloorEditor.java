package com.example.bd.ui.floor;

import com.example.bd.model.Floor;
import com.example.bd.repository.FloorRepository;
import com.example.bd.repository.HousingRepository;
import com.example.bd.repository.RoomRepository;
import com.example.bd.ui.BaseEditor;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.DateTimeFormatter;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in FloorWebView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX.
 */
//todo crud
@SpringComponent
@UIScope
public class FloorEditor extends VerticalLayout implements KeyNotifier, BaseEditor {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    private final RoomRepository roomRepository;
    private final FloorRepository floorRepository;
    private final HousingRepository housingRepository;

    private ChangeHandler changeHandler;


    private Floor floor;

    /*FIELDS*/
    private TextField idField = new TextField("ID");
    private TextField floorNumberField = new TextField("Floor number");
    private TextField housingIdField = new TextField("Housing");
    private TextField roomsField = new TextField("Rooms");

    /*BUTTONS*/
    private Button save = new Button("Save", VaadinIcon.CHECK.create());
    private Button update = new Button("Update", VaadinIcon.UPLOAD.create());
    private Button cancel = new Button("Cancel");
    private Button delete = new Button("Delete", VaadinIcon.TRASH.create());


    private HorizontalLayout actions = new HorizontalLayout(save, cancel, delete, update);

    Binder<Floor> binder = new Binder<>(Floor.class);


    @Autowired
    public FloorEditor(RoomRepository roomRepository, FloorRepository floorRepository, HousingRepository housingRepository) {
        this.roomRepository = roomRepository;
        this.floorRepository = floorRepository;
        this.housingRepository = housingRepository;

        add(idField, floorNumberField, housingIdField, roomsField, actions);


        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());
        addKeyPressListener(Key.ESCAPE, e -> changeHandler.onChange());


        save.addClickListener(e -> save());
        update.addClickListener(e -> update());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editFloor(floor));
        setVisible(false);
    }


    @Override
    public void save() {

        changeHandler.onChange();
    }


    @Override
    public void delete() {

        changeHandler.onChange();
    }


    @Override
    public void update() {


        changeHandler.onChange();
    }


    public final void editFloor(Floor floor) {
        if (floor == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = floor.getFloorId() != null;
        if (persisted) {
            this.floor = floorRepository.findById(floor.getFloorId()).get();
        } else {
            this.floor = floor;
        }

        // cancel.setVisible(persisted);

        cancel.setVisible(true);


        customSetupFields();


        setVisible(true);


        idField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {

        changeHandler = h;
    }


    @Override
    public void customSetupFields() {
        if (floor == null) {
            return;
        }

        if (floor.getFloorId() != null) {
            idField.setValue(floor.getFloorId().toString());
        }

        if (floor.getHousing() != null) {
            housingIdField.setValue(floor.getHousing().getHousingId().toString());
        }

        //todo edit this
        if (floor.getRooms() != null) {
            roomsField.setValue(floor.getRooms().toString());
        }

    }

    @Override
    public void initFromFields() {

    }

    public interface ChangeHandler {
        void onChange();
    }
}
