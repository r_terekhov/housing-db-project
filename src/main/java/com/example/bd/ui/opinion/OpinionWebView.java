package com.example.bd.ui.opinion;

import com.example.bd.model.Opinion;
import com.example.bd.repository.OpinionRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("opinion")
public class OpinionWebView extends VerticalLayout {

	private final OpinionRepository opinionRepository;

	private final OpinionEditor editor;

	private final Grid<Opinion> grid;

	private final TextField filter;

	private final Button addNewBtn;

	public OpinionWebView(OpinionRepository opinionRepository, OpinionEditor editor) {
		this.opinionRepository = opinionRepository;
		this.editor = editor;
		this.grid = new Grid<>(Opinion.class);
		this.filter = new TextField();
		this.addNewBtn = new Button("New opinion", VaadinIcon.PLUS.create());

		// build layout
		HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
		add(actions, grid, editor);

		grid.setHeight("300px");
		grid.setColumns("opinionId", "opinion", "mark", "date", "category", "extraInfo");
		grid.getColumnByKey("opinionId").setWidth("100px").setFlexGrow(0);

		filter.setPlaceholder("Filter by mark");

		// Hook logic to components

		// Replace listing with filtered content when user changes filter
		filter.setValueChangeMode(ValueChangeMode.EAGER);
		filter.addValueChangeListener(e -> listOpinions(e.getValue()));

		// Connect selected Customer to editor or hide if none is selected
		grid.asSingleSelect().addValueChangeListener(e -> editor.editOpinion(e.getValue()));

		// Instantiate and edit new Customer the new button is clicked
		addNewBtn.addClickListener(e -> editor.editOpinion(new Opinion()));

		// Listen changes made by the editor, refresh data from backend
		editor.setChangeHandler(() -> {
			editor.setVisible(false);
			listOpinions(filter.getValue());
		});

		// Initialize listing
		listOpinions(null);
	}


	private void listOpinions(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			grid.setItems(opinionRepository.findAll());
		} else {
			grid.setItems(opinionRepository.findAllByMark(Integer.valueOf(filterText)));
		}
	}

}
