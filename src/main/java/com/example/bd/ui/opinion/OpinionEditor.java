package com.example.bd.ui.opinion;

import com.example.bd.model.Opinion;
import com.example.bd.model.People;
import com.example.bd.repository.OpinionRepository;
import com.example.bd.repository.PeopleRepository;
import com.example.bd.ui.BaseEditor;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Optional;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in FloorWebView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX.
 */
@SpringComponent
@UIScope
public class OpinionEditor extends VerticalLayout implements KeyNotifier, BaseEditor {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    private final OpinionRepository opinionRepository;
    private final PeopleRepository peopleRepository;
    private ChangeHandler changeHandler;


    private Opinion opinion;

    /*FIELDS*/
    private TextField opinionField = new TextField("Opinion");
    private TextField markField = new TextField("Mark");
    private TextField dateField = new TextField("Date");
    private TextField categoryField = new TextField("Category");
    private TextField extraField = new TextField("Extra Info");
    private TextField authorField = new TextField("Author id");


    /*BUTTONS*/
    private Button save = new Button("Save", VaadinIcon.CHECK.create());
    private Button update = new Button("Update", VaadinIcon.UPLOAD.create());
    private Button cancel = new Button("Cancel");
    private Button delete = new Button("Delete", VaadinIcon.TRASH.create());


    private HorizontalLayout actions = new HorizontalLayout(save, cancel, delete, update);

    Binder<Opinion> binder = new Binder<>(Opinion.class);


    @Autowired
    public OpinionEditor(OpinionRepository opinionRepository, PeopleRepository peopleRepository) {
        this.opinionRepository = opinionRepository;
        this.peopleRepository = peopleRepository;

        add(opinionField, markField, dateField, categoryField, extraField, authorField, actions);

       /* // bind using naming convention
        binder.forField(this.markField)
                .withNullRepresentation("")
                .withConverter(new StringToIntegerConverter(1, "integers only"))
                .bind(Opinion::getMark, Opinion::setMark);

        binder.forField(this.dateField)
                .withNullRepresentation(LocalDate.now().format(formatter))
                .withConverter(new StringToLocalDateConverter())
                .bind(Opinion::getDate, Opinion::setDate);


        binder.bindInstanceFields(categoryField);

        binder.bindInstanceFields(extraField);*/

        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());
        addKeyPressListener(Key.ESCAPE, e -> changeHandler.onChange());


        save.addClickListener(e -> save());
        update.addClickListener(e -> update());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editOpinion(opinion));
        setVisible(false);
    }

    @Override
    public void delete() {

        customDelete();
        opinionRepository.deleteInBatch(Collections.singleton(opinion));
        changeHandler.onChange();
    }

    @Override
    public void save() {
        customSave();
    }

    @Override
    public void update() {
        customUpdate();
    }


    public final void editOpinion(Opinion opinion) {
        if (opinion == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = opinion.getOpinionId() != null;
        if (persisted) {
            this.opinion = opinionRepository.findById(opinion.getOpinionId()).get();
        } else {
            this.opinion = opinion;
        }

        // cancel.setVisible(persisted);

        cancel.setVisible(true);

        // Bind customer properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        //binder.setBean(opinion);
        customSetupFields();


        setVisible(true);

        // Focus first name initially
        opinionField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        // ChangeHandler is notified when either save or delete
        // is clicked
        changeHandler = h;
    }


    private void customSave() {
        String authorId = authorField.getValue();
        Long aLong = Long.valueOf(authorId);

        Optional<People> peopleOptional = peopleRepository.findById(aLong);

        if (!peopleOptional.isPresent()) {
            Notification.show("Невозможно сохранить/обновить/удалить мнение, так как пользователь с id " + authorId + " не найден");
            return;
        }


        opinion.setOpinion(opinionField.getValue());
        opinion.setMark(Integer.valueOf(markField.getValue()));
        opinion.setCategory(categoryField.getValue());
        opinion.setExtraInfo(extraField.getValue());
        opinion.setDate(LocalDate.parse(dateField.getValue(), formatter));
        opinion.setPeopleAsAuthor(peopleOptional.get());

        peopleRepository.save(peopleOptional.get());

        changeHandler.onChange();
    }


    private void customUpdate() {
        opinion.setOpinion(opinionField.getValue());
        opinion.setMark(Integer.valueOf(markField.getValue()));
        opinion.setDate(LocalDate.parse(dateField.getValue(), formatter));
        opinion.setCategory(categoryField.getValue());
        opinion.setExtraInfo(extraField.getValue());


        opinionRepository.save(opinion);
        changeHandler.onChange();

    }

    private void customDelete() {
        String authorId = authorField.getValue();
        Long aLong = Long.valueOf(authorId);

        Optional<People> peopleOptional = peopleRepository.findById(aLong);

        if (!peopleOptional.isPresent()) {
            Notification.show("Невозможно сохранить/обновить/удалить мнение, так как пользователь с id " + authorId + " не найден");
            return;
        }

        People people = peopleOptional.get();
        people.removeOpinion(opinion);

        peopleRepository.save(people);
    }

    @Override
    public void customSetupFields() {
        if (opinion == null) {
            return;
        }

        if (opinion.getOpinion() != null) {
            opinionField.setValue(opinion.getOpinion());
        }

        if (opinion.getCategory() != null) {
            categoryField.setValue(opinion.getCategory());
        }

        if (opinion.getExtraInfo() != null) {
            extraField.setValue(opinion.getExtraInfo());
        }

        if (opinion.getMark() != null) {
            markField.setValue(opinion.getMark().toString());
        }

        if (opinion.getDate() != null) {
            dateField.setValue(opinion.getDate().format(formatter));
        }

        if (opinion.getPeople() != null) {
            authorField.setValue(opinion.getPeople().getPeopleId().toString());
        }
    }

    @Override
    public void initFromFields() {

    }

    public interface ChangeHandler {
        void onChange();
    }
}
