package com.example.bd.ui.housing;

import com.example.bd.model.Housing;
import com.example.bd.model.Opinion;
import com.example.bd.repository.FloorRepository;
import com.example.bd.repository.HousingRepository;
import com.example.bd.ui.BaseEditor;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.DateTimeFormatter;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in FloorWebView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX.
 */
//todo crud
@SpringComponent
@UIScope
public class HousingEditor extends VerticalLayout implements KeyNotifier, BaseEditor {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    private final HousingRepository housingRepository;
    private final FloorRepository floorRepository;

    private ChangeHandler changeHandler;


    private Housing housing;

    /*FIELDS*/
    private TextField idField = new TextField("ID");
    private TextField typeField = new TextField("Type");
    private TextField floorsInTheBuildingField = new TextField("Floors in the building");
    private TextField numberOfRoomsField = new TextField("Number of rooms");
    private TextField roomsOnTheFloorField = new TextField("Rooms on the floor");
    private TextField roomCapacityField = new TextField("Room capacity field");


    /*BUTTONS*/
    private Button save = new Button("Save", VaadinIcon.CHECK.create());
    private Button update = new Button("Update", VaadinIcon.UPLOAD.create());
    private Button cancel = new Button("Cancel");
    private Button delete = new Button("Delete", VaadinIcon.TRASH.create());


    private HorizontalLayout actions = new HorizontalLayout(save, cancel, delete, update);

    Binder<Opinion> binder = new Binder<>(Opinion.class);


    @Autowired
    public HousingEditor(HousingRepository housingRepository, FloorRepository floorRepository) {
        this.housingRepository = housingRepository;
        this.floorRepository = floorRepository;

        add(idField, typeField, floorsInTheBuildingField, numberOfRoomsField, roomsOnTheFloorField, roomCapacityField, actions);


        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());
        addKeyPressListener(Key.ESCAPE, e -> changeHandler.onChange());


        save.addClickListener(e -> save());
        update.addClickListener(e -> update());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editHousing(housing));
        setVisible(false);
    }


    @Override
    public void save() {

        changeHandler.onChange();
    }


    @Override
    public void delete() {

        changeHandler.onChange();
    }


    @Override
    public void update() {


        changeHandler.onChange();
    }


    public final void editHousing(Housing housing) {
        if (housing == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = housing.getHousingId() != null;
        if (persisted) {
            this.housing = housingRepository.findById(housing.getHousingId()).get();
        } else {
            this.housing = housing;
        }

        // cancel.setVisible(persisted);

        cancel.setVisible(true);


        customSetupFields();


        setVisible(true);


        idField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {

        changeHandler = h;
    }


    @Override
    public void customSetupFields() {
        if (housing == null) {
            return;
        }

        if (housing.getHousingId() != null) {
            idField.setValue(housing.getHousingId().toString());
        }

        if (housing.getFloorsInTheBuilding() != null) {
            floorsInTheBuildingField.setValue(housing.getFloorsInTheBuilding().toString());
        }

        if (housing.getNumberOfRooms() != null) {
            numberOfRoomsField.setValue(housing.getNumberOfRooms().toString());
        }

        if (housing.getRoomCapacity() != null) {
            roomCapacityField.setValue(housing.getRoomCapacity().toString());
        }

        if (housing.getRoomsOnTheFloor() != null) {
            roomsOnTheFloorField.setValue(housing.getRoomsOnTheFloor().toString());
        }

        if (housing.getType() != null) {
            typeField.setValue(housing.getType().toString());
        }

    }

    @Override
    public void initFromFields() {

    }

    public interface ChangeHandler {
        void onChange();
    }
}
