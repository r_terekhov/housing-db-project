package com.example.bd.ui.housing;

import com.example.bd.model.Housing;
import com.example.bd.repository.HousingRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("housing")
public class HousingWebView extends VerticalLayout {

    private final HousingRepository housingRepository;

    private final HousingEditor editor;

    private final Grid<Housing> grid;

    private final TextField filter;

    private final Button addNewBtn;

    public HousingWebView(HousingRepository housingRepository, HousingEditor editor) {
        this.housingRepository = housingRepository;
        this.editor = editor;
        this.grid = new Grid<>(Housing.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New housing", VaadinIcon.PLUS.create());

        // build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("300px");
        grid.setColumns("housingId", "type", "floorsInTheBuilding", "numberOfRooms", "roomsOnTheFloor", "roomCapacity");
        grid.getColumnByKey("housingId").setWidth("100px").setFlexGrow(0);

        filter.setPlaceholder("Filter by type");

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listHousings(e.getValue()));

        // Connect selected Customer to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> editor.editHousing(e.getValue()));

        // Instantiate and edit new Customer the new button is clicked
        addNewBtn.addClickListener(e -> editor.editHousing(new Housing()));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listHousings(filter.getValue());
        });

        // Initialize listing
        listHousings(null);
    }


    private void listHousings(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(housingRepository.findAll());
        } else {
            grid.setItems(housingRepository.findAllByType(Integer.valueOf(filterText)));
        }
    }

}
