package com.example.bd.ui.people;

import com.example.bd.model.People;
import com.example.bd.repository.PeopleRepository;
import com.example.bd.ui.BaseEditor;
import com.example.bd.ui.StringToLocalDateConverter;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

@SpringComponent
@UIScope
public class PeopleEditor extends VerticalLayout implements KeyNotifier, BaseEditor {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    private final PeopleRepository peopleRepository;
    private ChangeHandler changeHandler;


    private People people;

    /*FIELDS*/
    private TextField peopleIdField = new TextField("PeopleId");
    private TextField peopleNameField = new TextField("Name");
    private TextField peopleLastName = new TextField("Last Name");
    private TextField createdField = new TextField("Created");

    /*BUTTONS*/
    private Button save = new Button("Save", VaadinIcon.CHECK.create());
    private Button update = new Button("Update", VaadinIcon.UPLOAD.create());
    private Button cancel = new Button("Cancel");
    private Button delete = new Button("Delete", VaadinIcon.TRASH.create());


    private HorizontalLayout actions = new HorizontalLayout(save, cancel, delete, update);

    Binder<People> binder = new Binder<>(People.class);


    @Autowired
    public PeopleEditor(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;

        add(peopleIdField, peopleNameField, peopleLastName, createdField, actions);

        // bind using naming convention
       /* binder.forField(this.markField)
                .withNullRepresentation("")
                .withConverter(new StringToIntegerConverter(1, "integers only"))
                .bind(Opinion::getMark, Opinion::setMark);*/

        binder.forField(this.createdField)
                .withNullRepresentation(LocalDate.now().format(formatter))
                .withConverter(new StringToLocalDateConverter())
                .bind(People::getCreated, People::setCreated);


        binder.bindInstanceFields(this);

        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());
        addKeyPressListener(Key.ESCAPE, e -> changeHandler.onChange());


        save.addClickListener(e -> save());
        update.addClickListener(e -> update());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editPeople(people));
        setVisible(false);
    }


    @Override
    public void save() {
        initFromFields();
        peopleRepository.save(people);
        changeHandler.onChange();
    }


    @Override
    public void delete() {
        peopleRepository.deleteInBatch(Collections.singleton(people));
        changeHandler.onChange();
    }


    @Override
    public void update() {
        initFromFields();
        peopleRepository.save(people);
        changeHandler.onChange();
    }


    public final void editPeople(People people) {
        if (people == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = people.getPeopleId() != null;
        if (persisted) {
            this.people = peopleRepository.findById(people.getPeopleId()).get();
        } else {
            this.people = people;
        }

        // cancel.setVisible(persisted);

        cancel.setVisible(true);


        binder.setBean(people);
        customSetupFields();


        setVisible(true);

        // Focus first name initially
        peopleNameField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        changeHandler = h;
    }


    @Override
    public void customSetupFields() {
        if (people == null) {
            return;
        }

        if (people.getName() != null) {
            peopleNameField.setValue(people.getName());
        }

        if (people.getLastName() != null) {
            peopleLastName.setValue(people.getLastName());
        }

        if (people.getCreated() != null) {
            createdField.setValue(people.getCreated().format(formatter));
        }


        if (people.getPeopleId() != null) {
            peopleIdField.setValue(people.getPeopleId().toString());
        }
    }

    @Override
    public void initFromFields() {
        people.setName(peopleNameField.getValue());
        people.setLastName(peopleLastName.getValue());
        people.setCreated(LocalDate.now());
    }

    public interface ChangeHandler {
        void onChange();
    }
}
