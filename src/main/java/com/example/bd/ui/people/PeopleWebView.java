package com.example.bd.ui.people;

import com.example.bd.model.People;
import com.example.bd.repository.PeopleRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("people")
public class PeopleWebView extends VerticalLayout {

    private final PeopleRepository peopleRepository;

    private final PeopleEditor editor;

    private final Grid<People> grid;

    private final TextField filter;

    private final Button addNewBtn;

    public PeopleWebView(PeopleRepository peopleRepository, PeopleEditor editor) {
        this.peopleRepository = peopleRepository;
        this.editor = editor;
        this.grid = new Grid<>(People.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New people", VaadinIcon.PLUS.create());

        // build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("300px");
        grid.setColumns("peopleId", "name", "lastName", "created");
        grid.getColumnByKey("peopleId").setWidth("100px").setFlexGrow(0);

        filter.setPlaceholder("Filter by last name");

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listOpinions(e.getValue()));

        // Connect selected Customer to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> editor.editPeople(e.getValue()));

        // Instantiate and edit new Customer the new button is clicked
        addNewBtn.addClickListener(e -> editor.editPeople(new People()));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listOpinions(filter.getValue());
        });

        // Initialize listing
        listOpinions(null);
    }


    private void listOpinions(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(peopleRepository.findAll());
        } else {
            grid.setItems(peopleRepository.findAllByLastNameStartingWith(filterText));
        }
    }
}
