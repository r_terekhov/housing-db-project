package com.example.bd.ui.room;

import com.example.bd.model.Opinion;
import com.example.bd.model.Room;
import com.example.bd.repository.FloorRepository;
import com.example.bd.repository.HousingRepository;
import com.example.bd.repository.RoomRepository;
import com.example.bd.ui.BaseEditor;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.DateTimeFormatter;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in FloorWebView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX.
 */
//todo crud
@SpringComponent
@UIScope
public class RoomEditor extends VerticalLayout implements KeyNotifier, BaseEditor {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

    private final RoomRepository roomRepository;
    private final FloorRepository floorRepository;
    private final HousingRepository housingRepository;

    private ChangeHandler changeHandler;


    private Room room;

    /*FIELDS*/
    private TextField idField = new TextField("ID");
    private TextField floorIdField = new TextField("Floor");
    private TextField housingIdField = new TextField("Housing");
    private TextField capacityField = new TextField("Capacity");
    private TextField busyField = new TextField("Busy");
    private TextField bookedFromField = new TextField("Booked from");
    private TextField bookedToField = new TextField("Booked to");


    /*BUTTONS*/
    private Button save = new Button("Save", VaadinIcon.CHECK.create());
    private Button update = new Button("Update", VaadinIcon.UPLOAD.create());
    private Button cancel = new Button("Cancel");
    private Button delete = new Button("Delete", VaadinIcon.TRASH.create());


    private HorizontalLayout actions = new HorizontalLayout(save, cancel, delete, update);

    Binder<Opinion> binder = new Binder<>(Opinion.class);


    @Autowired
    public RoomEditor(RoomRepository roomRepository, FloorRepository floorRepository, HousingRepository housingRepository) {
        this.roomRepository = roomRepository;
        this.floorRepository = floorRepository;
        this.housingRepository = housingRepository;

        add(idField, floorIdField, housingIdField, capacityField, busyField, bookedFromField, bookedToField, actions);


        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());
        addKeyPressListener(Key.ESCAPE, e -> changeHandler.onChange());


        save.addClickListener(e -> save());
        update.addClickListener(e -> update());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editRoom(room));
        setVisible(false);
    }


    @Override
    public void save() {

        changeHandler.onChange();
    }


    @Override
    public void delete() {

        changeHandler.onChange();
    }


    @Override
    public void update() {


        changeHandler.onChange();
    }


    public final void editRoom(Room room) {
        if (room == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = room.getRoomId() != null;
        if (persisted) {
            this.room = roomRepository.findById(room.getRoomId()).get();
        } else {
            this.room = room;
        }

        // cancel.setVisible(persisted);

        cancel.setVisible(true);


        customSetupFields();


        setVisible(true);


        idField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {

        changeHandler = h;
    }


    @Override
    public void customSetupFields() {
        if (room == null) {
            return;
        }

        if (room.getRoomId() != null) {
            idField.setValue(room.getRoomId().toString());
        }

        if (room.getHousing() != null) {
            housingIdField.setValue(room.getHousing().getHousingId().toString());
        }

        if (room.getFloor() != null) {
            floorIdField.setValue(room.getFloor().getFloorId().toString());
        }

        if (room.getCapacity() != null) {
            capacityField.setValue(room.getCapacity().toString());
        }

        if (room.getBusy() != null) {
            busyField.setValue(room.getBusy().toString());
        }

        if (room.getBookedFrom() != null) {
            bookedFromField.setValue(room.getBookedFrom().format(formatter));
        }

        if (room.getBookedTo() != null) {
            bookedToField.setValue(room.getBookedTo().format(formatter));
        }

    }

    @Override
    public void initFromFields() {

    }

    public interface ChangeHandler {
        void onChange();
    }
}
