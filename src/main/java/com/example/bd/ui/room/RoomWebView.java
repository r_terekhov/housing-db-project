package com.example.bd.ui.room;

import com.example.bd.model.Room;
import com.example.bd.repository.RoomRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("room")
public class RoomWebView extends VerticalLayout {

    private final RoomRepository roomRepository;

    private final RoomEditor editor;

    private final Grid<Room> grid;

    private final TextField filter;

    private final Button addNewBtn;

    public RoomWebView(RoomRepository roomRepository, RoomEditor editor) {
        this.roomRepository = roomRepository;
        this.editor = editor;
        this.grid = new Grid<>(Room.class);
        this.filter = new TextField();
        this.addNewBtn = new Button("New room", VaadinIcon.PLUS.create());

        // build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        add(actions, grid, editor);

        grid.setHeight("300px");
        grid.setColumns("roomId", "floor", "housing", "capacity", "busy", "bookedFrom", "bookedTo");
        grid.getColumnByKey("roomId").setWidth("100px").setFlexGrow(0);

        filter.setPlaceholder("Filter by busy");

        // Hook logic to components

        // Replace listing with filtered content when user changes filter
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listRooms(e.getValue()));

        // Connect selected Customer to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> editor.editRoom(e.getValue()));

        // Instantiate and edit new Customer the new button is clicked
        addNewBtn.addClickListener(e -> editor.editRoom(new Room()));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listRooms(filter.getValue());
        });

        // Initialize listing
        listRooms(null);
    }


    private void listRooms(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(roomRepository.findAll());
        } else {
            //todo remove hardcode
            grid.setItems(roomRepository.findAllByBusy(false));
        }
    }

}
