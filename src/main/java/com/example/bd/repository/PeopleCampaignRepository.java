package com.example.bd.repository;

import com.example.bd.model.Campaign;
import com.example.bd.model.connection.PeopleCampaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeopleCampaignRepository extends JpaRepository<PeopleCampaign, Long> {

    List<PeopleCampaign> findAllByCampaign(Campaign campaign);
}
