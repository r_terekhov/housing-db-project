package com.example.bd.repository;

import com.example.bd.model.Opinion;
import com.example.bd.model.People;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OpinionRepository extends JpaRepository<Opinion, Long> {

    List<Opinion> findAllByMarkLessThan(int badMark);

    List<Opinion> findAllByPeopleAndMarkLessThan(People people, int badMark);

    List<Opinion> findAllByMark(Integer mark);

}
