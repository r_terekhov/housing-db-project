package com.example.bd.repository;

import com.example.bd.model.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ContractRepository extends JpaRepository<Contract, Long> {

    List<Contract> findAllByStartDateAfterAndEndDateBefore(LocalDate startPeriod, LocalDate endPeriod);


    List<Contract> findAllByStartDateAfterAndEndDateBeforeAndVolumeIsGreaterThanEqual(LocalDate startPeriod, LocalDate endPeriod, Integer volume);

    List<Contract> findAllByVolumeIsGreaterThanEqual(Integer volume);
}
