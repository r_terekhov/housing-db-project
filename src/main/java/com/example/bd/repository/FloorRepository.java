package com.example.bd.repository;

import com.example.bd.model.Floor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FloorRepository extends JpaRepository<Floor, Long> {

    List<Floor> findAllByFloorNumber(Integer floorNumber);
}
