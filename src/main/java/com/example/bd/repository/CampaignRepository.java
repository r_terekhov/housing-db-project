package com.example.bd.repository;

import com.example.bd.model.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long> {

    Campaign findCampaignByNameLike(String campaignName);

    List<Campaign> findAllByNameStartingWith(String prefix);
}
