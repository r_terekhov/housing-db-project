package com.example.bd.repository;

import com.example.bd.model.Housing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HousingRepository extends JpaRepository<Housing, Long> {

    @Override
    List<Housing> findAll();

    List<Housing> findAllByType(Integer type);
}
