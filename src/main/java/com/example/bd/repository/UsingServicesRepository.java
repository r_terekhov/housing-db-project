package com.example.bd.repository;

import com.example.bd.model.People;
import com.example.bd.model.Room;
import com.example.bd.model.UsingServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface UsingServicesRepository extends JpaRepository<UsingServices, Long> {

    List<UsingServices> findAllByPeopleAndRoom(People people, Room room);

    List<UsingServices> findAllByPeopleAndRoomAndDateBetween(People people, Room room, LocalDate start, LocalDate end);
}
