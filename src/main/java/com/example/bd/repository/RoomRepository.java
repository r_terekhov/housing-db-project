package com.example.bd.repository;

import com.example.bd.model.Room;
import com.example.bd.model.connection.RoomRoomOptions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    long countAllByBusyFalse();

    List<Room> findAllByBusyFalseAndRoomRoomOptionsIn(List<RoomRoomOptions> roomRoomOptions);

    Optional<Room> findRoomByRoomIdAndBusyFalse(Long id);

    List<Room> findAllByBusyTrueAndBookedToBefore(LocalDate neededDate);

    Room findRoomByRoomId(Long roomId);

    List<Room> findAllByBusy(Boolean busy);


}
