package com.example.bd.repository;

import com.example.bd.model.RoomOptions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RoomOptionsRepository extends JpaRepository<RoomOptions, Long> {

    List<RoomOptions> findAllByDescriptionIn(Set<String> descriptions);
}
