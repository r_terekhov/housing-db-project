package com.example.bd.repository;

import com.example.bd.model.Arrival;
import com.example.bd.model.People;
import com.example.bd.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ArrivalRepository extends JpaRepository<Arrival, Long> {

    List<Arrival> findAllByStartPeriodAfterAndEndPeriodBefore(LocalDate startPeriod, LocalDate endPeriod);

    List<Arrival> findFirst10ByStartPeriodAfterAndEndPeriodBeforeAndPeopleInOrderByRoom(LocalDate startPeriod, LocalDate endPeriod, List<People> people);

    List<Arrival> findAllByPeople(People people);

    List<Arrival> findAllByStartPeriodAfterAndEndPeriodBeforeAndRoom(LocalDate startPeriod, LocalDate endPeriod, Room room);

    List<Arrival> findAllByPeopleInAndWasBookedTrue(List<People> people);

}
