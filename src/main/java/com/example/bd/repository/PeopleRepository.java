package com.example.bd.repository;

import com.example.bd.model.People;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Repository
public interface PeopleRepository extends JpaRepository<People, Long> {

    Set<People> findAllByPeopleIdIn(List<Integer> ids);

    People findPeopleByNameLikeAndLastNameLike(String name, String lastName);

    List<People> findAllByPeopleIdInAndCreatedBetween(Set<Long> ids, LocalDate currentDateWithMinus, LocalDate currentDate);

    List<People> findAllByLastNameStartingWith(String prefix);
}
