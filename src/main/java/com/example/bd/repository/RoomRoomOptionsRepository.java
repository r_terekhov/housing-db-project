package com.example.bd.repository;

import com.example.bd.model.Room;
import com.example.bd.model.RoomOptions;
import com.example.bd.model.connection.RoomRoomOptions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRoomOptionsRepository extends JpaRepository<RoomRoomOptions, Long> {


    List<RoomRoomOptions> findAllByRoomOptionsIn(List<RoomOptions> roomOptions);

    List<RoomRoomOptions> findAllByRoom(Room room);

    Optional<RoomRoomOptions> findRoomRoomOptionsByRoom(Room room);
}
