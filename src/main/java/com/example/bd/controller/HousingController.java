/*
package com.example.bd.controller;

import com.example.bd.model.Floor;
import com.example.bd.model.Housing;
import com.example.bd.service.impl.FloorService;
import com.example.bd.service.impl.HousingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "housing")
public class HousingController {
    private final HousingService housingService;
    private final FloorService floorService;

    public HousingController(HousingService housingService, FloorService floorService) {
        this.housingService = housingService;
        this.floorService = floorService;
    }

    @GetMapping(value = "new")
    public ResponseEntity<Housing> saveNewHousing() {
        Housing housing = new Housing(0, 5, 25, 5, 3);
        Floor floor = new Floor(4, housing);
        Floor floor1 = floorService.saveNewFloor(floor);
        //housing.addFlor(floor1);
        return new ResponseEntity<>(housingService.createHousing(housing), HttpStatus.OK);

    }

}
*/
