package com.example.bd.controller;

import com.example.bd.dto.*;
import com.example.bd.model.Room;
import com.example.bd.service.impl.ArrivalAndPeopleService;
import com.example.bd.service.impl.PeopleAndCampaignService;
import com.example.bd.service.impl.PeopleAndOpinionService;
import com.example.bd.service.impl.RoomService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@RestController
public class AllSelectsController {

    private final RoomService roomService;
    private final ArrivalAndPeopleService arrivalAndPeopleService;
    private final PeopleAndCampaignService peopleAndCampaignService;
    private final PeopleAndOpinionService peopleAndOpinionService;

    public AllSelectsController(RoomService roomService, ArrivalAndPeopleService arrivalAndPeopleService, PeopleAndCampaignService peopleAndCampaignService, PeopleAndOpinionService peopleAndOpinionService) {
        this.roomService = roomService;
        this.arrivalAndPeopleService = arrivalAndPeopleService;
        this.peopleAndCampaignService = peopleAndCampaignService;
        this.peopleAndOpinionService = peopleAndOpinionService;
    }


    //1 запрос

    @GetMapping("first_part_1")
    public List<CampaignView> first_part_1(
            @RequestParam String startPeriod,
            @RequestParam String endPeriod,
            @RequestParam Integer volume
    ) {
        return peopleAndCampaignService.getCampaignsFromContractsByPeriodAndVolumeNotLessThan(startPeriod, endPeriod, volume);
    }


    @GetMapping("first_part_2")
    public List<CampaignView> first_part_2(
            @RequestParam Integer volume
    ) {
        return peopleAndCampaignService.getCampaignsFromContractsVolumeNotLessThan(volume);
    }

    //2 запрос
    //http://localhost:8086/countPeoplesAndReturnListByPeriod?startPeriod=2019-06-01&endPeriod=2019-06-30
    @GetMapping("countPeoplesAndReturnListByPeriod")
    public ArrivalsListAndCountByPeriod getPeoplesByPeriod(
            @RequestParam String startPeriod, @RequestParam String endPeriod) {

        return arrivalAndPeopleService.getPeoplesByPeriod(startPeriod, endPeriod);
    }


    //3 запрос
    @GetMapping("countFreeRooms")
    public Long countFreeRooms() {
        return roomService.countAllFreeRooms();
    }

    //4 запрос - фулл
    //todo вывести все характеристики и сгененрить формочку, в которую передать по названию опции номера и сделать пост хапрос
    //его открыть в новом окне
    @GetMapping("getInfoAboutFreeRoomsAndOptions")
    public List<Room> getInfoAboutFreeRoomsAndOptions() {
        //todo remove hardcode
        Set<String> roomOptions = Collections.singleton("сауна");
        return roomService.getInfoAboutFreeRoomsWithOptions(roomOptions);
    }

    //4 запрос
    @GetMapping("countFreeRoomsWithCertainOptions")
    public Integer countFreeRoomsWithCertainOptions() {
        return getInfoAboutFreeRoomsAndOptions().size();
    }

    //5 запрос
    @GetMapping("getInfoAboutCertainRoom/{roomId}")
    public RoomInfo getInfoAboutCertainRoom(@PathVariable String roomId) {
        return roomService.getInfoAboutRoom(Long.valueOf(roomId));
    }

    //6 запрос
    @GetMapping("getAllBusyRoomsWhichWillBeFreeTo/{date}")
    public List<RoomView> getAllBusyRoomsWhichWillBeFreeTo(@PathVariable String date) {
        return roomService.getAllBusyRoomsWhichWillBeFreeTo(date);
    }

    //7 запрос
    //http://localhost:8086/getInfoAboutBookingByCampaign?startPeriod=2019-06-01&endPeriod=2019-06-30&campaignName=%D0%90%D0%99%D0%92%D0%9E%D0%99%D0%A1
    @GetMapping("getInfoAboutBookingByCampaign")
    public ArrivalsByCampaign getInfoAboutBookingByCampaign(
            @RequestParam String startPeriod,
            @RequestParam String endPeriod,
            @RequestParam String campaignName) {
        return peopleAndCampaignService.getInfoAboutBookingByCampaign(campaignName, startPeriod, endPeriod);
    }

    //8 запрос
    @GetMapping("getBadOpinions")
    public List<BadOpinion> getBadOpinions() {
        return peopleAndOpinionService.getBadOpinions();
    }


    //10 запрос
    @GetMapping("getInfoAboutPeopleFromRoomHisServicesAndBadOpinions")
    public TenSelectDto getInfoAboutPeopleFromRoomHisServicesAndBadOpinions(
            @RequestParam String name,
            @RequestParam String lastName,
            @RequestParam Long roomId) {
        return arrivalAndPeopleService.getInfoAboutPeopleFromRoomHisServicesAndBadOpinins(name, lastName, roomId);
    }


    //11 запрос
    @GetMapping("getInfoAboutContracts")
    List<ContractView> getInfoAboutContracts(@RequestParam String startPeriod,
                                             @RequestParam String endPeriod) {
        return peopleAndCampaignService.getInfoAboutContracts(startPeriod, endPeriod);
    }

    //13 запрос

    @GetMapping("getNewPeoples")
    List<PeopleView> getNewPeoples() {
        return arrivalAndPeopleService.getNewPeoples();
    }

    //14 запрос


    @GetMapping("getInfoAboutCertainPeople")
    public _14SelectDto getInfoAboutCertainPeople(@RequestParam Long peopleId) {
        return arrivalAndPeopleService.getInfoAboutCertainPeople(peopleId);
    }

    //15 запрос
    @GetMapping("getInfoAboutCertainRoom")
    public List<_15SelectDto> getInfoAboutCertainRoom(
            @RequestParam Long roomId,
            @RequestParam String startPeriod,
            @RequestParam String endPeriod) {
        return arrivalAndPeopleService.getInfoAboutCertainRoom(roomId, startPeriod, endPeriod);
    }

    //16 запрос
    @GetMapping("getProportion")
    public List<String> getProportion() {
        return peopleAndCampaignService.proportion();
    }

}
