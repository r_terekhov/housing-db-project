package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArrivalsListAndCountByPeriod {
    private Integer count;
    private Set<PeopleView> peoples;
}
