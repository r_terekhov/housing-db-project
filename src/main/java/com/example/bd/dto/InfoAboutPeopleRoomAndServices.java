package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * класс для 14 селекта
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InfoAboutPeopleRoomAndServices {
    private RoomView roomView;
    private ServiceView serviceView;
    private String dateStart;
    private String dateEnd;
}
