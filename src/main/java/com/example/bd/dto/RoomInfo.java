package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomInfo {
    private Long roomId;
    private String willFree;
    private Set<String> roomOptions;
}
