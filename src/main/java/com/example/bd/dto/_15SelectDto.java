package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class _15SelectDto {
    PeopleView peopleView;
    String startDate;
    String endDate;
}
