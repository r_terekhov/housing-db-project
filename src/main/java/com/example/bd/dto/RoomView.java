package com.example.bd.dto;

/* Получить список занятых сейчас номеров, которые освобождаются
к указанному сроку.
*/

import com.example.bd.model.Room;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomView {
    private Long roomId;

    private Integer capacity;


    public static RoomView of(Room room) {
        return new RoomView(room.getRoomId(), room.getCapacity());
    }
}
