package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArrivalsByCampaign {
    private Integer size;
    private List<RoomView> info;
}
