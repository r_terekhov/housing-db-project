package com.example.bd.dto;

import com.example.bd.model.Opinion;
import com.example.bd.model.People;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadOpinion {
    private Long peopleId;
    private String name;
    private String lastName;

    private String opinion;
    private Integer mark;


    public static BadOpinion of(Opinion opinion) {
        People people = opinion.getPeople();
        return new BadOpinion(people.getPeopleId(), people.getName(), people.getLastName(), opinion.getOpinion(), opinion.getMark());
    }
}
