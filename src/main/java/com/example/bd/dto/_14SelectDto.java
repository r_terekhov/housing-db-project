package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * класс для 14 селекта
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class _14SelectDto {
    private Integer amountOfArrivals;
    private List<InfoAboutPeopleRoomAndServices> infoAboutPeopleRoomAndServices;
}
