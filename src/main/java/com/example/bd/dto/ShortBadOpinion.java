package com.example.bd.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShortBadOpinion {
    private String opinion;
    private Integer mark;
}
