package com.example.bd.dto;

import com.example.bd.model.People;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeopleView {
    private Long id;
    private String name;
    private String lastName;


    public static PeopleView of(People people) {
        return new PeopleView(people.getPeopleId(), people.getName(), people.getLastName());
    }
}
