package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractView {

    private CampaignView campaignView;

    private String startPeriod;

    private String endPeriod;
}
