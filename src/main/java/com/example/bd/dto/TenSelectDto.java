package com.example.bd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenSelectDto {
    private PeopleView peopleView;
    private RoomView roomView;
    private List<ServiceView> serviceViews;
    private Integer totalCost;

    private List<ShortBadOpinion> badOpinions;
}
