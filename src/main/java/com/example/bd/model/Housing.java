package com.example.bd.model;

import com.example.bd.model.connection.HousingService;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Entity
@Table(name = "housing")
@Data
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "housingId")
public class Housing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long housingId;

    @Column(name = "type")
    private Integer type;

    @Column(name = "floors_in_the_building")
    private Integer floorsInTheBuilding;

    @Column(name = "number_of_rooms")
    private Integer numberOfRooms;

    @Column(name = "rooms_on_the_floor")
    private Integer roomsOnTheFloor;

    @Column(name = "room_capacity")
    private Integer roomCapacity;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "housing")
    private Set<Floor> floors = new HashSet<>();

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "housing")
    private Set<Room> rooms = new HashSet<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "housing")
    Set<HousingService> housingServices = new HashSet<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "housing")
    Set<Arrival> arrivals = new HashSet<>();


    public Housing(Integer type, Integer floorsInTheBuilding, Integer numberOfRooms, Integer roomsOnTheFloor, Integer roomCapacity, Set<Floor> floors) {
        this.type = type;
        this.floorsInTheBuilding = floorsInTheBuilding;
        this.numberOfRooms = numberOfRooms;
        this.roomsOnTheFloor = roomsOnTheFloor;
        this.roomCapacity = roomCapacity;
        this.floors = floors;
    }

    public Housing(Integer type, Integer floorsInTheBuilding, Integer numberOfRooms, Integer roomsOnTheFloor, Integer roomCapacity) {
        this.type = type;
        this.floorsInTheBuilding = floorsInTheBuilding;
        this.numberOfRooms = numberOfRooms;
        this.roomsOnTheFloor = roomsOnTheFloor;
        this.roomCapacity = roomCapacity;
    }

    public void addFlor(Floor floor) {
        if (floors.contains(floor)) {
            return;
        }

        floors.add(floor);

        floor.setHousing(this);
    }

    public void removeFlor(Floor floor) {
        if (floors.contains(floor)) {
            return;
        }

        floors.remove(floor);

        floor.removeHousing(this);
    }


    @Override
    public String toString() {
        return "Housing{" +
                "housingId=" + housingId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Housing)) return false;
        Housing housing = (Housing) o;
        return getHousingId().equals(housing.getHousingId()) &&
                getType().equals(housing.getType()) &&
                getFloorsInTheBuilding().equals(housing.getFloorsInTheBuilding()) &&
                getNumberOfRooms().equals(housing.getNumberOfRooms()) &&
                getRoomsOnTheFloor().equals(housing.getRoomsOnTheFloor()) &&
                getRoomCapacity().equals(housing.getRoomCapacity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHousingId(), getType(), getFloorsInTheBuilding(), getNumberOfRooms(), getRoomsOnTheFloor(), getRoomCapacity());
    }
}
