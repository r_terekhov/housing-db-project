package com.example.bd.model;


import com.example.bd.model.connection.PeopleCampaign;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "campaign")
@NoArgsConstructor
@Data
public class Campaign {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long campaignId;

    @Column(name = "name")
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy = "campaign")
    Set<PeopleCampaign> peopleCampaigns = new HashSet<>();

    @Override
    public String toString() {
        return "Campaign{" +
                "campaignId=" + campaignId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Campaign)) return false;
        Campaign campaign = (Campaign) o;
        return getCampaignId().equals(campaign.getCampaignId()) &&
                getName().equals(campaign.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCampaignId(), getName());
    }
}
