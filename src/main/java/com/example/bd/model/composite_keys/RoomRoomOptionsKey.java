package com.example.bd.model.composite_keys;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@Data
public class RoomRoomOptionsKey implements Serializable {

    @Column(name = "roomId")
    private Long roomId;

    @Column(name = "roomOptionId")
    private Long roomOptionId;


    @Override
    public String toString() {
        return "RoomRoomOptionsKey{" +
                "roomId=" + roomId +
                ", roomOptionId=" + roomOptionId +
                '}';
    }
}
