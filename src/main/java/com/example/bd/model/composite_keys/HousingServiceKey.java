package com.example.bd.model.composite_keys;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@Data
public class HousingServiceKey implements Serializable {

    @Column(name = "housingId")
    private Long housingId;

    @Column(name = "serviceId")
    private Long serviceId;

}
