package com.example.bd.model.composite_keys;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@Data
public class PeopleCampaignKey implements Serializable {

    @Column(name = "peopleId")
    private Long peopleId;

    @Column(name = "campaignId")
    private Long campaignId;


}
