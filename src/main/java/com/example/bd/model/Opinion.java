package com.example.bd.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "opinion")
@Data
public class Opinion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long opinionId;

    @Column(name = "opinion")
    private String opinion;


    @Min(value = 1, message = "Mark should not be less than 1")
    @Max(value = 5, message = "Mark should not be greater than 5")
    @Column(name = "mark")
    private Integer mark;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "category")
    private String category;


    @Column(name = "extra_info")
    private String extraInfo;

    @ManyToOne
    @JoinColumn(name = "peopleId", nullable = false)
    //@OnDelete(action = OnDeleteAction.CASCADE)
    private People people;

    public Opinion(String opinion, Integer mark, String category, String extraInfo) {
        this.opinion = opinion;
        this.mark = mark;
        this.date = LocalDate.now();
        this.category = category;
        this.extraInfo = extraInfo;
    }

    public Opinion() {
        this.date = LocalDate.now();
    }

    @Override
    public String toString() {
        return "Opinion{" +
                "opinionId=" + opinionId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Opinion)) return false;
        Opinion opinion1 = (Opinion) o;
        return getOpinionId().equals(opinion1.getOpinionId()) &&
                getOpinion().equals(opinion1.getOpinion()) &&
                getMark().equals(opinion1.getMark()) &&
                getDate().equals(opinion1.getDate()) &&
                Objects.equals(getCategory(), opinion1.getCategory()) &&
                Objects.equals(getExtraInfo(), opinion1.getExtraInfo());
    }

    public void setPeopleAsAuthor(People people) {
        this.setPeople(people);

        people.addNewOpinion(this);

    }





    @Override
    public int hashCode() {
        return Objects.hash(getOpinionId(), getOpinion(), getMark(), getDate(), getCategory(), getExtraInfo());
    }
}
