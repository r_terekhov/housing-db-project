package com.example.bd.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Entity
@Table(name = "arrival")
@Data
public class Arrival {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long arrivalId;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "peopleId")
    People people;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "housingId")
    Housing housing;

    @ManyToOne
    /*   @JsonBackReference*/
    @JsonManagedReference
    @JoinColumn(name = "roomId")
    Room room;

    @Column(name = "start_period")
    private LocalDate startPeriod;

    @Column(name = "end_period")
    private LocalDate endPeriod;

    @Column(name = "was_booked")
    private Boolean wasBooked;


    @Override
    public String toString() {
        return "Arrival{" +
                "arrivalId=" + arrivalId +
                '}';
    }
}
