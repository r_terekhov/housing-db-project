package com.example.bd.model.connection;

import com.example.bd.model.Housing;
import com.example.bd.model.Service;
import com.example.bd.model.composite_keys.HousingServiceKey;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "housing_services")
public class HousingService {

    @EmbeddedId
    private HousingServiceKey id;

    @ManyToOne
    @JsonBackReference
    @MapsId("housingId")
    @JoinColumn(name = "housingId")
    private Housing housing;

    @ManyToOne
    @JsonBackReference
    @MapsId("serviceId")
    @JoinColumn(name = "serviceId")
    private Service service;

    @Override
    public String toString() {
        return "HousingService{" +
                "id=" + id +
                '}';
    }
}
