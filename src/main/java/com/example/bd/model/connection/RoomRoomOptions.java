package com.example.bd.model.connection;


import com.example.bd.model.Room;
import com.example.bd.model.RoomOptions;
import com.example.bd.model.composite_keys.RoomRoomOptionsKey;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Data
@Table(name = "room_options_and_room")
public class RoomRoomOptions {

    @EmbeddedId
    private RoomRoomOptionsKey id;

    @ManyToOne
    @JsonBackReference
    @MapsId("roomId")
    @JoinColumn(name = "roomId")
    private Room room;

    @ManyToOne
    @JsonBackReference
    @MapsId("roomOptionId")
    @JoinColumn(name = "roomOptionId")
    private RoomOptions roomOptions;


    @Override
    public String toString() {
        return "RoomRoomOptions{" +
                "room=" + room +
                ", roomOptions=" + roomOptions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomRoomOptions)) return false;
        RoomRoomOptions that = (RoomRoomOptions) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
