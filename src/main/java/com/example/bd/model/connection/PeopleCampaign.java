package com.example.bd.model.connection;


import com.example.bd.model.Campaign;
import com.example.bd.model.People;
import com.example.bd.model.composite_keys.PeopleCampaignKey;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "people_campaign")
public class PeopleCampaign {

    @EmbeddedId
    private PeopleCampaignKey id;

    @ManyToOne
    @JsonBackReference
    @MapsId("peopleId")
    @JoinColumn(name = "peopleId")
    private People people;

    @ManyToOne
    @JsonBackReference
    @MapsId("campaignId")
    @JoinColumn(name = "campaignId")
    private Campaign campaign;

    @Override
    public String toString() {
        return "PeopleCampaign{" +
                "id=" + id +
                '}';
    }
}
