package com.example.bd.model;

import com.example.bd.model.connection.PeopleCampaign;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "people")
@Data
public class People {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long peopleId;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "created")
    private LocalDate created;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, mappedBy = "people")
    private Set<Opinion> opinions;

    @JsonManagedReference
    @OneToMany(mappedBy = "people")
    Set<PeopleCampaign> peopleCampaigns;

    @JsonManagedReference
    @OneToMany(mappedBy = "people")
    Set<Arrival> arrivals;

    @JsonManagedReference
    @OneToMany(mappedBy = "people")
    Set<UsingServices> usingServices;

    public People() {
        created = LocalDate.now();
    }

    @Override
    public String toString() {
        return "People{" +
                "peopleId=" + peopleId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof People)) return false;
        People people = (People) o;
        return getPeopleId().equals(people.getPeopleId()) &&
                getName().equals(people.getName()) &&
                getLastName().equals(people.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPeopleId(), getName(), getLastName());
    }


    public void addNewOpinion(Opinion opinion) {
        //prevent endless loop
        if (opinions.contains(opinion))
            return;
        //add new account
        opinions.add(opinion);
        //set myself into the twitter account
        opinion.setPeopleAsAuthor(this);
    }


    public void removeOpinion(Opinion opinion) {
        //prevent endless loop
        if (!opinions.contains(opinion))
            return;

        opinions.remove(opinion);

    }




}
