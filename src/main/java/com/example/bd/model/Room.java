package com.example.bd.model;

import com.example.bd.model.connection.RoomRoomOptions;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
@Table(name = "room")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "roomId")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long roomId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "floorId", nullable = false)
    private Floor floor;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "housingId", nullable = false)
    private Housing housing;

    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "busy")
    private Boolean busy;

    @Column(name = "booked_from")
    private LocalDate bookedFrom;

    @Column(name = "booked_to")
    private LocalDate bookedTo;


    //@JsonManagedReference
    /*@JsonIgnore*/
    @JsonBackReference
    @OneToMany(mappedBy = "room")
    Set<RoomRoomOptions> roomRoomOptions;

    /* @JsonManagedReference*/
    @JsonBackReference
    @OneToMany(mappedBy = "room")
    Set<Arrival> arrivals;

    @JsonManagedReference
    @OneToMany(mappedBy = "room")
    Set<UsingServices> usingServices;

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return getRoomId().equals(room.getRoomId()) &&
                getCapacity().equals(room.getCapacity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRoomId(), getCapacity());
    }
}
