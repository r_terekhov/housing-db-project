package com.example.bd.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@Data
@Table(name = "use_service")
public class UsingServices {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transactionId;


    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "roomId")
    private Room room;


    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "peopleId")
    private People people;


    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "serviceId")
    private Service service;

    @Column(name = "date")
    private LocalDate date;

    @Override
    public String toString() {
        return "UsingServices{" +
                "transactionId=" + transactionId +
                '}';
    }
}
