package com.example.bd.model;


import com.example.bd.model.connection.HousingService;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "service")
@NoArgsConstructor
@Data
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long serviceId;

    @Column(name = "name")
    private String name;

    @Column(name = "cost")
    private Integer cost;


    @Column(name = "category")
    private String category;

    @JsonManagedReference
    @OneToMany(mappedBy = "service")
    Set<HousingService> housingServices;

    @JsonManagedReference
    @OneToMany(mappedBy = "service")
    Set<UsingServices> usingServices;

    @Override
    public String toString() {
        return "Service{" +
                "serviceId=" + serviceId +
                '}';
    }
}
