package com.example.bd.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Entity
@Table(name = "floor")
@Data
@NoArgsConstructor
public class Floor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long floorId;

    @Column(name = "floor_number")
    private Integer floorNumber;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "housingId", nullable = false)
    private Housing housing;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "floor")
    private Set<Room> rooms;

    public Floor(Integer floorNumber, Housing housing) {
        this.floorNumber = floorNumber;
        this.housing = housing;
    }

    public Floor(Integer floorNumber) {
        this.floorNumber = floorNumber;
    }


    public void setupHousing(Housing housing) {
        if (sameAsFormer(housing)) {
            return;
        }

        //set new housing
        Housing oldHousing = this.housing;
        this.housing = housing;
        //remove from the old facebook account
        if (oldHousing != null)
            oldHousing.removeFlor(this);
        //set myself into new facebook account
        if (housing != null)
            housing.addFlor(this);
    }


    public void removeHousing(Housing housing) {
        housing.removeFlor(this);
    }

    private boolean sameAsFormer(Housing newHousing) {
        return Objects.equals(housing, newHousing);
    }

    @Override
    public String toString() {
        return "Floor{" +
                "floorId=" + floorId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Floor)) return false;
        Floor floor = (Floor) o;
        return getFloorId().equals(floor.getFloorId()) &&
                getFloorNumber().equals(floor.getFloorNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFloorId(), getFloorNumber());
    }
}
