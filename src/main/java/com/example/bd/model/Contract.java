package com.example.bd.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Data
@Entity
@Table(name = "contract")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long contractId;

    @ManyToOne
    @JoinColumn(name = "campaignId")
    private Campaign campaign;

    @ManyToOne
    @JoinColumn(name = "housingId")
    private Housing housing;

    //скидка
    @Column(name = "sale")
    private Integer sale;

    @Column(name = "start_date")
    private LocalDate startDate;


    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "volume")
    private Integer volume;


    @Override
    public String toString() {
        return "Contract{" +
                "contractId=" + contractId +
                '}';
    }
}
