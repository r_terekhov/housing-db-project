package com.example.bd.model;


import com.example.bd.model.connection.RoomRoomOptions;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "room_options")
@NoArgsConstructor
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "roomOptionId")
public class RoomOptions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long roomOptionId;

    @Column(name = "description")
    private String description;


    @JsonManagedReference
    @OneToMany(mappedBy = "roomOptions")
    Set<RoomRoomOptions> roomRoomOptions;

    @Override
    public String toString() {
        return "RoomOptions{" +
                "roomOptionId=" + roomOptionId +
                '}';
    }
}
