package com.example.bd.service.impl;

import com.example.bd.model.Floor;
import com.example.bd.repository.FloorRepository;
import org.springframework.stereotype.Service;

@Service
public class FloorService {

    private final FloorRepository floorRepository;

    public FloorService(FloorRepository floorRepository) {
        this.floorRepository = floorRepository;
    }

    public Floor saveNewFloor(Floor floor) {
        return floorRepository.save(floor);
    }

}
