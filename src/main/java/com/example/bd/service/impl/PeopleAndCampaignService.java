package com.example.bd.service.impl;

import com.example.bd.dto.ArrivalsByCampaign;
import com.example.bd.dto.CampaignView;
import com.example.bd.dto.ContractView;
import com.example.bd.dto.RoomView;
import com.example.bd.model.Arrival;
import com.example.bd.model.Campaign;
import com.example.bd.model.Contract;
import com.example.bd.model.People;
import com.example.bd.model.connection.PeopleCampaign;
import com.example.bd.repository.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class PeopleAndCampaignService {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
    private final ArrivalRepository arrivalRepository;
    private final CampaignRepository campaignRepository;
    private final PeopleCampaignRepository peopleCampaignRepository;
    private final PeopleRepository peopleRepository;
    private final ContractRepository contractRepository;

    public PeopleAndCampaignService(ArrivalRepository arrivalRepository, CampaignRepository campaignRepository, PeopleCampaignRepository peopleCampaignRepository, PeopleRepository peopleRepository, ContractRepository contractRepository) {
        this.arrivalRepository = arrivalRepository;
        this.campaignRepository = campaignRepository;
        this.peopleCampaignRepository = peopleCampaignRepository;
        this.peopleRepository = peopleRepository;
        this.contractRepository = contractRepository;
    }

    //7 запрос

    public ArrivalsByCampaign getInfoAboutBookingByCampaign(String campaignName, String startPeriod, String endPeriod) {
        Campaign campaign = campaignRepository.findCampaignByNameLike(campaignName);
        if (campaign == null) {
            return null;
        }

        List<PeopleCampaign> peopleCampaignList = peopleCampaignRepository.findAllByCampaign(campaign);

        List<People> people = new ArrayList<>();

        peopleCampaignList.forEach(peopleCampaign -> {
            people.add(peopleCampaign.getPeople());
        });


        List<Arrival> arrivals = arrivalRepository.findFirst10ByStartPeriodAfterAndEndPeriodBeforeAndPeopleInOrderByRoom(LocalDate.parse(startPeriod, formatter), LocalDate.parse(endPeriod, formatter), people);

        int size = arrivals.size();
        List<RoomView> info = new ArrayList<>();

        arrivals.forEach(arrival -> {
            info.add(RoomView.of(arrival.getRoom()));
        });


        return new ArrivalsByCampaign(size, info);



    /*    List<Room> rooms = new ArrayList<>();

        arrivals.forEach(arrival -> {
            rooms.add(arrival.getRoom());
        });

        Set<Room> uniqueRooms = new HashSet<>();*/


    }

    //11 запрос
    public List<ContractView> getInfoAboutContracts(String startPeriod, String endPeriod) {
        LocalDate start = LocalDate.parse(startPeriod, formatter);
        LocalDate end = LocalDate.parse(endPeriod, formatter);

        List<Contract> contractList = contractRepository.findAllByStartDateAfterAndEndDateBefore(start, end);

        List<ContractView> contractViewList = new ArrayList<>();

        contractList.forEach(contract -> {

            contractViewList.add(new ContractView(new CampaignView(contract.getCampaign().getCampaignId(), contract.getCampaign().getName()),
                    contract.getStartDate().format(formatter),
                    contract.getEndDate().format(formatter)));
        });

        return contractViewList;

    }

    //16 запрос
    public List<String> proportion() {
        List<PeopleCampaign> peopleCampaignList = peopleCampaignRepository.findAll();

        List<Arrival> arrivals = arrivalRepository.findAll();

        Double totalArrivals = Double.valueOf(arrivals.size());

        List<People> people = new ArrayList<>();

        peopleCampaignList.forEach(peopleCampaign -> {
            people.add(peopleCampaign.getPeople());
        });

        List<Arrival> arrivalRepositoryAllByPeopleInAndWasBookedTrue = arrivalRepository.findAllByPeopleInAndWasBookedTrue(people);

        Double wasBookedByCampaigns = Double.valueOf(arrivalRepositoryAllByPeopleInAndWasBookedTrue.size());

        List<String> result = new ArrayList<>();

        result.add(totalArrivals.toString());
        result.add((wasBookedByCampaigns.toString()));
        result.add(String.valueOf(wasBookedByCampaigns / totalArrivals * 100));

        return result;
    }

    //1 запрос_период
    public List<CampaignView> getCampaignsFromContractsByPeriodAndVolumeNotLessThan(String startPeriod, String endPeriod, Integer volume) {
        LocalDate start = LocalDate.parse(startPeriod, formatter);
        LocalDate end = LocalDate.parse(endPeriod, formatter);

        List<Contract> contractList = contractRepository.findAllByStartDateAfterAndEndDateBeforeAndVolumeIsGreaterThanEqual(start, end, volume);


        List<CampaignView> campaignViews = new ArrayList<>();


        contractList.forEach(contract -> {
            campaignViews.add(new CampaignView(contract.getCampaign().getCampaignId(),
                    contract.getCampaign().getName()));
        });

        return campaignViews;
    }


    //1запрос_за всё время

    public List<CampaignView> getCampaignsFromContractsVolumeNotLessThan(Integer volume) {


        List<Contract> contractList = contractRepository.findAllByVolumeIsGreaterThanEqual(volume);


        List<CampaignView> campaignViews = new ArrayList<>();


        contractList.forEach(contract -> {
            campaignViews.add(new CampaignView(contract.getCampaign().getCampaignId(),
                    contract.getCampaign().getName()));
        });

        return campaignViews;
    }

}
