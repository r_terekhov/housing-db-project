package com.example.bd.service.impl;

import com.example.bd.dto.RoomInfo;
import com.example.bd.dto.RoomView;
import com.example.bd.model.Room;
import com.example.bd.model.RoomOptions;
import com.example.bd.model.connection.RoomRoomOptions;
import com.example.bd.repository.RoomOptionsRepository;
import com.example.bd.repository.RoomRepository;
import com.example.bd.repository.RoomRoomOptionsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@Transactional
public class RoomService {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
    private final RoomRepository roomRepository;
    private final RoomRoomOptionsRepository roomRoomOptionsRepository;
    private final RoomOptionsRepository roomOptionsRepository;

    public RoomService(RoomRepository roomRepository, RoomRoomOptionsRepository roomRoomOptionsRepository, RoomOptionsRepository roomOptionsRepository) {
        this.roomRepository = roomRepository;
        this.roomRoomOptionsRepository = roomRoomOptionsRepository;
        this.roomOptionsRepository = roomOptionsRepository;
    }

    //3
    public long countAllFreeRooms() {
        return roomRepository.countAllByBusyFalse();
    }

    //todo add parameters
    //4
    public List<Room> getInfoAboutFreeRoomsWithOptions(Set<String> options) {
        List<RoomOptions> roomOptions = roomOptionsRepository.findAllByDescriptionIn(options);
        List<RoomRoomOptions> roomRoomOptions = roomRoomOptionsRepository.findAllByRoomOptionsIn(roomOptions);
        return roomRepository.findAllByBusyFalseAndRoomRoomOptionsIn(roomRoomOptions);
    }


    //5
    public RoomInfo getInfoAboutRoom(Long roomId) {
        Optional<Room> optionalRoom = roomRepository.findRoomByRoomIdAndBusyFalse(roomId);
        if (!optionalRoom.isPresent()) {
            return null;
        }

        Room room = optionalRoom.get();

        List<RoomRoomOptions> roomRoomOptions = roomRoomOptionsRepository.findAllByRoom(room);

        Set<String> roomOptions = new HashSet<>();

        roomRoomOptions.forEach(roomRoomOptions1 -> {
            roomOptions.add(roomRoomOptions1.getRoomOptions().getDescription());
        });


        String willBeFree = howLongRoomWillBeFree(room.getBookedFrom().format(formatter), room.getBookedTo().format(formatter));
        return new RoomInfo(roomId, willBeFree, roomOptions);

    }

    //6
    public List<RoomView> getAllBusyRoomsWhichWillBeFreeTo(String date) {
        List<RoomView> roomViews = new ArrayList<>();

        roomRepository.findAllByBusyTrueAndBookedToBefore(getDateFromString(date)).forEach(room -> {
            roomViews.add(RoomView.of(room));
        });

        return roomViews;
    }


    private LocalDate getDateFromString(String date) {
        return LocalDate.parse(date, formatter);
    }


    private String howLongRoomWillBeFree(String startPeriod, String endPeriod) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);

        if (startPeriod == null || endPeriod == null) {
            return "infinity";
        }

        LocalDate startDate = LocalDate.parse(startPeriod, formatter);

        LocalDate now = LocalDate.now();

        long between = DAYS.between(now, startDate);

        return String.valueOf(between);
    }
}
