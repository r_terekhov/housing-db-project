package com.example.bd.service.impl;

import com.example.bd.dto.*;
import com.example.bd.model.*;
import com.example.bd.repository.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ArrivalAndPeopleService {

    private static final String DATE_FORMATTER = "yyyy-MM-dd";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
    private final ArrivalRepository arrivalRepository;
    private final PeopleRepository peopleRepository;
    private final OpinionRepository opinionRepository;
    private final UsingServicesRepository usingServicesRepository;
    private final RoomRepository roomRepository;

    public ArrivalAndPeopleService(ArrivalRepository arrivalRepository, PeopleRepository peopleRepository, OpinionRepository opinionRepository, UsingServicesRepository usingServicesRepository, RoomRepository roomRepository) {
        this.arrivalRepository = arrivalRepository;
        this.peopleRepository = peopleRepository;
        this.opinionRepository = opinionRepository;
        this.usingServicesRepository = usingServicesRepository;
        this.roomRepository = roomRepository;
    }

    public ArrivalsListAndCountByPeriod getPeoplesByPeriod(String startPeriod, String endPeriod) {
        LocalDate start = LocalDate.parse(startPeriod, formatter);
        LocalDate end = LocalDate.parse(endPeriod, formatter);
        List<Arrival> arrivalList = arrivalRepository.findAllByStartPeriodAfterAndEndPeriodBefore(start, end);


        Set<PeopleView> peopleSet = new HashSet<>();
        arrivalList.forEach(arrival -> peopleSet.add(PeopleView.of(arrival.getPeople())));

        return new ArrivalsListAndCountByPeriod(peopleSet.size(), peopleSet);
    }


    /*10. Получить сведения о постояльце из заданного номера: его счет
гостинице за дополнительные услуги, поступавшие от него жалобы,
виды дополнительных услуг, которыми он пользовался.

*/
    public TenSelectDto getInfoAboutPeopleFromRoomHisServicesAndBadOpinins(String name, String lastName, Long roomId) {

        People people = peopleRepository.findPeopleByNameLikeAndLastNameLike(name, lastName);

        if (people == null) {
            return null;
        }

        Room room = roomRepository.findRoomByRoomId(roomId);

        if (room == null) {
            return null;
        }


        List<UsingServices> usingServices = usingServicesRepository.findAllByPeopleAndRoom(people, room);

        List<ServiceView> serviceViews = new ArrayList<>();
        AtomicReference<Integer> totalCost = new AtomicReference<>(0);

        usingServices.forEach(usingServices1 -> {
            com.example.bd.model.Service service = usingServices1.getService();
            totalCost.updateAndGet(v -> v + service.getCost());
            serviceViews.add(new ServiceView(service.getName(), service.getCost()));
        });

        List<Opinion> opinions = opinionRepository.findAllByPeopleAndMarkLessThan(people, 3);

        List<ShortBadOpinion> shortBadOpinionList = new ArrayList<>();

        opinions.forEach(opinion -> shortBadOpinionList.add(new ShortBadOpinion(opinion.getOpinion(), opinion.getMark())));

        return new TenSelectDto(
                new PeopleView(people.getPeopleId(), people.getName(), people.getLastName()),
                new RoomView(room.getRoomId(), room.getCapacity()),
                serviceViews,
                totalCost.get(),
                shortBadOpinionList);


    }


    /*13. Получить сведения о новых клиентах за указанный период.
     */
    public List<PeopleView> getNewPeoples() {
        LocalDate currentDate = LocalDate.now();
        LocalDate currentDateWithMinus = currentDate.minusDays(30);
        List<Arrival> arrivalList = arrivalRepository.findAllByStartPeriodAfterAndEndPeriodBefore(currentDateWithMinus, currentDate);

        Set<Long> people = new HashSet<>();

        arrivalList.forEach(arrival -> {
            people.add(arrival.getPeople().getPeopleId());
        });

        List<People> peopleList = peopleRepository.findAllByPeopleIdInAndCreatedBetween(people, currentDate, currentDateWithMinus);

        List<PeopleView> peopleViewList = new ArrayList<>();

        peopleList.forEach(people1 -> {
            peopleViewList.add(PeopleView.of(people1));
        });

        return peopleViewList;
    }

    /*14. Получить сведения о конкретном человеке, сколько раз он посещал
гостиницу, в каких номерах и в какой период останавливался, какие
счета оплачивал.
*/

    public _14SelectDto getInfoAboutCertainPeople(Long peopleId) {

        Optional<People> optionalPeople = peopleRepository.findById(peopleId);

        if (!optionalPeople.isPresent()) {
            return null;
        }

        People certainPeople = optionalPeople.get();

        List<Arrival> arrivalList = arrivalRepository.findAllByPeople(certainPeople);

        Integer amountOfArrivals = arrivalList.size();

        List<InfoAboutPeopleRoomAndServices> infoAboutPeopleRoomAndServices = new ArrayList<>();

        arrivalList.forEach(arrival -> {

            Room room = arrival.getRoom();
            List<UsingServices> usingServices = usingServicesRepository.findAllByPeopleAndRoomAndDateBetween(certainPeople, room, arrival.getStartPeriod(), arrival.getEndPeriod());

            usingServices.forEach(usingServices1 -> {
                infoAboutPeopleRoomAndServices.add(new InfoAboutPeopleRoomAndServices(
                        RoomView.of(room),
                        new ServiceView(usingServices1.getService().getName(), usingServices1.getService().getCost()),
                        arrival.getStartPeriod().format(formatter),
                        arrival.getEndPeriod().format(formatter)
                ));
            });
        });


        return new _14SelectDto(amountOfArrivals, infoAboutPeopleRoomAndServices);

    }

    //15 запрос

    /*
15. Получить сведения о конкретном номере: кем он был занят в
определенный период.
*/
    public List<_15SelectDto> getInfoAboutCertainRoom(Long roomId, String startPeriod, String endPeriod) {
        Room room = roomRepository.findRoomByRoomId(roomId);

        if (room == null) {
            return null;
        }

        final LocalDate dateStart = LocalDate.parse(startPeriod, formatter);
        final LocalDate dateEnd = LocalDate.parse(endPeriod, formatter);

        List<_15SelectDto> selectDtoList = new ArrayList<>();

        List<Arrival> arrivals = arrivalRepository.findAllByStartPeriodAfterAndEndPeriodBeforeAndRoom(dateStart, dateEnd, room);

        arrivals.forEach(arrival -> {
            selectDtoList.add(new _15SelectDto(PeopleView.of(arrival.getPeople()),
                    arrival.getStartPeriod().format(formatter),
                    arrival.getEndPeriod().format(formatter)));
        });

        return selectDtoList;

    }

}
