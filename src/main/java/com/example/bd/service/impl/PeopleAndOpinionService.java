package com.example.bd.service.impl;

import com.example.bd.dto.BadOpinion;
import com.example.bd.model.Opinion;
import com.example.bd.repository.OpinionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PeopleAndOpinionService {

    private final OpinionRepository opinionRepository;

    public PeopleAndOpinionService(OpinionRepository opinionRepository) {
        this.opinionRepository = opinionRepository;
    }

    public List<BadOpinion> getBadOpinions() {
        int badOpinion = 3;
        List<Opinion> opinions = opinionRepository.findAllByMarkLessThan(badOpinion);

        List<BadOpinion> badOpinions = new ArrayList<>();
        opinions.forEach(opinion -> badOpinions.add(BadOpinion.of(opinion)));

        return badOpinions;
    }
}
