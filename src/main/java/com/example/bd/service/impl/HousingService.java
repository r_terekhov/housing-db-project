package com.example.bd.service.impl;

import com.example.bd.model.Housing;
import com.example.bd.repository.HousingRepository;
import org.springframework.stereotype.Service;

@Service
public class HousingService {

    private final HousingRepository housingRepository;

    public HousingService(HousingRepository housingRepository) {
        this.housingRepository = housingRepository;
    }

    public Housing createHousing(Housing housing) {
        return housingRepository.save(housing);
    }

    public Housing updateHousing(Housing housing) {
        return housingRepository.save(housing);
    }
}
